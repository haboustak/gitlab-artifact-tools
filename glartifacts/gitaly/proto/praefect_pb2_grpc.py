# Generated by the gRPC Python protocol compiler plugin. DO NOT EDIT!
import grpc

from . import praefect_pb2 as praefect__pb2


class PraefectInfoServiceStub(object):
  """PraefectInfoService is a service which provides RPCs to query and modify
  Praefect-specific parameters.
  """

  def __init__(self, channel):
    """Constructor.

    Args:
      channel: A grpc.Channel.
    """
    self.RepositoryReplicas = channel.unary_unary(
        '/gitaly.PraefectInfoService/RepositoryReplicas',
        request_serializer=praefect__pb2.RepositoryReplicasRequest.SerializeToString,
        response_deserializer=praefect__pb2.RepositoryReplicasResponse.FromString,
        )
    self.DatalossCheck = channel.unary_unary(
        '/gitaly.PraefectInfoService/DatalossCheck',
        request_serializer=praefect__pb2.DatalossCheckRequest.SerializeToString,
        response_deserializer=praefect__pb2.DatalossCheckResponse.FromString,
        )
    self.SetAuthoritativeStorage = channel.unary_unary(
        '/gitaly.PraefectInfoService/SetAuthoritativeStorage',
        request_serializer=praefect__pb2.SetAuthoritativeStorageRequest.SerializeToString,
        response_deserializer=praefect__pb2.SetAuthoritativeStorageResponse.FromString,
        )
    self.MarkUnverified = channel.unary_unary(
        '/gitaly.PraefectInfoService/MarkUnverified',
        request_serializer=praefect__pb2.MarkUnverifiedRequest.SerializeToString,
        response_deserializer=praefect__pb2.MarkUnverifiedResponse.FromString,
        )
    self.SetReplicationFactor = channel.unary_unary(
        '/gitaly.PraefectInfoService/SetReplicationFactor',
        request_serializer=praefect__pb2.SetReplicationFactorRequest.SerializeToString,
        response_deserializer=praefect__pb2.SetReplicationFactorResponse.FromString,
        )
    self.GetRepositoryMetadata = channel.unary_unary(
        '/gitaly.PraefectInfoService/GetRepositoryMetadata',
        request_serializer=praefect__pb2.GetRepositoryMetadataRequest.SerializeToString,
        response_deserializer=praefect__pb2.GetRepositoryMetadataResponse.FromString,
        )


class PraefectInfoServiceServicer(object):
  """PraefectInfoService is a service which provides RPCs to query and modify
  Praefect-specific parameters.
  """

  def RepositoryReplicas(self, request, context):
    """This comment is left unintentionally blank.
    """
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def DatalossCheck(self, request, context):
    """DatalossCheck checks for unavailable repositories.
    """
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def SetAuthoritativeStorage(self, request, context):
    """SetAuthoritativeStorage sets the authoritative storage for a repository on a given virtual storage.
    This causes the current version of the repository on the authoritative storage to be considered the
    latest and overwrite any other version on the virtual storage.
    """
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def MarkUnverified(self, request, context):
    """MarkUnverified marks replicas as unverified. This will trigger verification as Praefect's metadata
    verifier prioritizes unverified replicas.
    """
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def SetReplicationFactor(self, request, context):
    """SetReplicationFactor assigns or unassigns host nodes from the repository to meet the desired replication factor.
    SetReplicationFactor returns an error when trying to set a replication factor that exceeds the storage node count
    in the virtual storage. An error is also returned when trying to set a replication factor below one. The primary node
    won't be unassigned as it needs a copy of the repository to accept writes. Likewise, the primary is the first storage
    that gets assigned when setting a replication factor for a repository. Assignments of unconfigured storages are ignored.
    This might cause the actual replication factor to be higher than desired if the replication factor is set during an upgrade
    from a Praefect node that does not yet know about a new node. As assignments of unconfigured storages are ignored, replication
    factor of repositories assigned to a storage node removed from the cluster is effectively decreased.
    """
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def GetRepositoryMetadata(self, request, context):
    """GetRepositoryMetadata returns the cluster metadata for a repository. Returns NotFound if the repository does not exist.
    """
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')


def add_PraefectInfoServiceServicer_to_server(servicer, server):
  rpc_method_handlers = {
      'RepositoryReplicas': grpc.unary_unary_rpc_method_handler(
          servicer.RepositoryReplicas,
          request_deserializer=praefect__pb2.RepositoryReplicasRequest.FromString,
          response_serializer=praefect__pb2.RepositoryReplicasResponse.SerializeToString,
      ),
      'DatalossCheck': grpc.unary_unary_rpc_method_handler(
          servicer.DatalossCheck,
          request_deserializer=praefect__pb2.DatalossCheckRequest.FromString,
          response_serializer=praefect__pb2.DatalossCheckResponse.SerializeToString,
      ),
      'SetAuthoritativeStorage': grpc.unary_unary_rpc_method_handler(
          servicer.SetAuthoritativeStorage,
          request_deserializer=praefect__pb2.SetAuthoritativeStorageRequest.FromString,
          response_serializer=praefect__pb2.SetAuthoritativeStorageResponse.SerializeToString,
      ),
      'MarkUnverified': grpc.unary_unary_rpc_method_handler(
          servicer.MarkUnverified,
          request_deserializer=praefect__pb2.MarkUnverifiedRequest.FromString,
          response_serializer=praefect__pb2.MarkUnverifiedResponse.SerializeToString,
      ),
      'SetReplicationFactor': grpc.unary_unary_rpc_method_handler(
          servicer.SetReplicationFactor,
          request_deserializer=praefect__pb2.SetReplicationFactorRequest.FromString,
          response_serializer=praefect__pb2.SetReplicationFactorResponse.SerializeToString,
      ),
      'GetRepositoryMetadata': grpc.unary_unary_rpc_method_handler(
          servicer.GetRepositoryMetadata,
          request_deserializer=praefect__pb2.GetRepositoryMetadataRequest.FromString,
          response_serializer=praefect__pb2.GetRepositoryMetadataResponse.SerializeToString,
      ),
  }
  generic_handler = grpc.method_handlers_generic_handler(
      'gitaly.PraefectInfoService', rpc_method_handlers)
  server.add_generic_rpc_handlers((generic_handler,))
