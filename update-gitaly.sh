#!/bin/sh
set -e

log() {
    stdbuf -e0 echo $@ 1>&2
}

log -n "Fetching submodule..."
git submodule update --init
log "OK"

rm -rf glartifacts/gitaly/proto
mkdir -p glartifacts/gitaly/proto
touch glartifacts/gitaly/proto/__init__.py

log -n "Generating python client..."
python -m grpc_tools.protoc \
    -Iext/gitaly/proto \
    --python_out=glartifacts/gitaly/proto \
    --grpc_python_out=glartifacts/gitaly/proto \
    ext/gitaly/proto/*.proto
log "OK"

# Rewrite gitaly to use relative imports
log -n "Rewwriting python import statements..."
sed -i \
	's/^import \([^ ]*\)_pb2 as \([^ ]*\)$/from . import \1_pb2 as \2/' \
	glartifacts/gitaly/proto/*.py
log "OK"
