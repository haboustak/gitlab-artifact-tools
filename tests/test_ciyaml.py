import unittest

import yaml

from glartifacts.ciyaml import GitLabYamlLoader, Reference

# "!reference tag example" by GitLab.com documentation
# https://docs.gitlab.com/ee/ci/yaml/yaml_optimization.html#reference-tags
# CC BY-SA 4.0 https://creativecommons.org/licenses/by-sa/4.0/
SIMPLE_YAML_REF = """
include:
  - local: configs.yml

.teardown:
  after_script:
    - echo deleting environment

test:
  script:
    - !reference [.setup, script]
    - echo running my own command
  after_script:
    - !reference [.teardown, after_script]
"""

# "Nested !reference tag example" by GitLab.com documentation
# https://docs.gitlab.com/ee/ci/yaml/yaml_optimization.html#reference-tags
# CC BY-SA 4.0 https://creativecommons.org/licenses/by-sa/4.0/
NESTED_YAML_REF = """
.snippets:
  one:
    - echo "ONE!"
  two:
    - !reference [.snippets, one]
    - echo "TWO!"
  three:
    - !reference [.snippets, two]
    - echo "THREE!"

nested-references:
  script:
    - !reference [.snippets, three]
"""


class TestCiYaml(unittest.TestCase):
    def test_should_load_simple_reference(self):
        try:
            config = yaml.load(SIMPLE_YAML_REF, Loader=GitLabYamlLoader)
        except yaml.constructor.ConstructorError:
            self.fail("ConstructorError raised by yaml.load")

        ref = config['test']['script'][0]
        self.assertEqual(type(ref), Reference)
        self.assertEqual(len(ref.path), 2)
        self.assertEqual(ref.path[0], '.setup')
        self.assertEqual(ref.path[1], 'script')

        ref = config['test']['after_script'][0]
        self.assertEqual(type(ref), Reference)
        self.assertEqual(len(ref.path), 2)
        self.assertEqual(ref.path[0], '.teardown')
        self.assertEqual(ref.path[1], 'after_script')

    def test_should_load_nested_reference(self):
        try:
            config = yaml.load(NESTED_YAML_REF, Loader=GitLabYamlLoader)
        except yaml.constructor.ConstructorError:
            self.fail("ConstructorError raised by yaml.load")

        first_ref = config['.snippets']['two'][0]
        self.assertEqual(type(first_ref), Reference)
        self.assertEqual(len(first_ref.path), 2)
        self.assertEqual(first_ref.path[0], '.snippets')
        self.assertEqual(first_ref.path[1], 'one')

        nested_ref = config['nested-references']['script'][0]
        self.assertEqual(type(nested_ref), Reference)
        self.assertEqual(len(nested_ref.path), 2)
        self.assertEqual(nested_ref.path[0], '.snippets')
        self.assertEqual(nested_ref.path[1], 'three')
